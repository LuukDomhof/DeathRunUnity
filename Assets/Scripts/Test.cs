﻿using UnityEngine;
using System.Collections;

public class Test : Photon.MonoBehaviour {

	public GameObject pivot;
	bool enabled;

	// Use this for initialization
	void Start () {
	
		pivot = GameObject.Find("Pivot");
	}
	
	// Update is called once per frame
	void Update () {
	
		pivot.GetComponent<Animator>().enabled = enabled;

		if(Input.GetKeyDown(KeyCode.O)){
				PhotonNetwork.RPC(photonView,"toggle",PhotonTargets.All,false, new object[0]);
		}

	}

	[PunRPC]
	void toggle(){
		enabled = !enabled;
	}
}
