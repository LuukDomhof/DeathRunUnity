﻿using UnityEngine;
using System.Collections;

public class Networking : Photon.MonoBehaviour {

	CharacterMovement charMovement;
	MouseLook cameraScript;
	MouseLook playerTurn;

	public GameObject camera;


	void Awake(){

		charMovement = GetComponent<CharacterMovement>();
		playerTurn = GetComponent<MouseLook>();
		cameraScript = camera.GetComponent<MouseLook>();

		if(photonView.isMine){
			charMovement.enabled = true;
			cameraScript.enabled = true;
			playerTurn.enabled = true;
			camera.GetComponent<Camera>().enabled = true;
		}else{
			charMovement.enabled = false;
			cameraScript.enabled = false;
			playerTurn.enabled = false;
			camera.GetComponent<Camera>().enabled = false;
		}

		gameObject.name = gameObject.name + photonView.viewID;
		
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		
		if(stream.isWriting){
			
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
		}
		else{

			correctPlayerPos = (Vector3)stream.ReceiveNext();
			correctPlayerRot = (Quaternion)stream.ReceiveNext();

		}
	}

	private Vector3 correctPlayerPos = Vector3.zero;
	private Quaternion correctPlayerRot = Quaternion.identity;

	// Update is called once per frame
	void Update () {
	
		if(!photonView.isMine){
			transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
			transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot,Time.deltaTime *5);
		}

	}
}
