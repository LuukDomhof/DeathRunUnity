﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	public float walkSpeed;
	public float runSpeed;
	public float jumpSpeed;

	public float walkPushPower;
	public float runPushPower;

	float currentSpeed;
	float pushPower = 2;
	public Vector3 moveDirection;

	float speedX;
	float speedY;
	float speedZ;

	public bool isGrounded;
	public float gravityModifier;

	CharacterController charCont;



	// Use this for initialization
	void Start () {
	
		charCont = this.gameObject.GetComponent<CharacterController>();

	}
	
	// Update is called once per frame
	void Update () {

		CheckButtons();
		Movement();
		Gravity();

		isGrounded = charCont.isGrounded;

	}

	void Movement ()
	{
		speedX = Input.GetAxis("Horizontal") * currentSpeed;
		speedZ = Input.GetAxis("Vertical") * currentSpeed;

		moveDirection = new Vector3(speedX,speedY,speedZ);

		moveDirection = transform.TransformDirection(moveDirection);

		charCont.Move(moveDirection);
	}

	void Gravity ()
	{
		if(!charCont.isGrounded){
			speedY += Physics.gravity.y * gravityModifier * Time.deltaTime;
		}

		if(charCont.isGrounded){
			speedY = 0;
		}
	}
	void CheckButtons ()
	{
		if(!Input.GetButton("Run")){
			currentSpeed = walkSpeed;
			pushPower = walkPushPower;
		}else{
			currentSpeed = runSpeed;
			pushPower = runPushPower;
		}

		if(charCont.isGrounded){
			if(Input.GetButton("Jump")){
				speedY += jumpSpeed;
			}
		}

		if(Input.GetButtonDown("Respawn")){
			this.gameObject.transform.position = GameObject.Find("Spawn").transform.position;
		}
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		Rigidbody body = hit.collider.attachedRigidbody;
		if (body == null || body.isKinematic)
			return;
		
		if (hit.moveDirection.y < -0.3F)
			return;
		
		Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
		body.velocity = pushDir * pushPower;
	}
}