﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using PlayFab;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class DeveloperPerks : Photon.MonoBehaviour {

	public List<string> keys = new List<string>();

	public UserDataRecord developer;

	public GameObject DevMenu;

	public GameObject playerGraphics;
	
	public bool isDev;

	public GameObject masterStatus;

	public object devString;

	public 

	ExitGames.Client.Photon.Hashtable propsToSet = new ExitGames.Client.Photon.Hashtable();


	// Use this for initialization
	void Start () {

		masterStatus = GameObject.Find ("MasterStatus");
		DevMenu = GameObject.Find ("DevMenu");
		DevMenu.SetActive(false);

		keys.Add ("Developer");

		GetUserDataRequest request = new GetUserDataRequest();

		request.Keys = keys;

		propsToSet.Add ("Developer", "True");

		PlayFabClientAPI.GetUserData(request, GetUserDataCallback,OnPlayFabError);

	}
	
	// Update is called once per frame
	void Update () {
	
		AmIMaster();

		if(Input.GetKeyDown(KeyCode.U)){
			Debug.Log (PhotonNetwork.player.customProperties.TryGetValue("Developer", out devString));
			Debug.Log("Is Dev? " + devString.ToString());
			if(DevMenu.GetActive() == true){
				DevMenu.SetActive(false);
			}else{
				DevMenu.SetActive(true);
			}
		}
	}

	void GetUserDataCallback (GetUserDataResult result)
	{
		Debug.Log ("Output: " + result.Data.TryGetValue("Developer", out developer));
		Debug.Log (developer.Value);
		if(developer.Value == "True" || developer.Value == "Yes"){
			isDev = true;
			PhotonNetwork.player.SetCustomProperties(propsToSet);
		}
	}

	void OnPlayFabError (PlayFabError error)
	{
		Debug.Log(error.ErrorMessage);
	}

	public void MakeMeMaster(){
		PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		Debug.Log ("Master: " + PhotonNetwork.masterClient);
	}

	void AmIMaster(){

		if(photonView.isMine){
			if(PhotonNetwork.isMasterClient){
				masterStatus.GetComponent<Text>().text = "I am master";
			}else{
				masterStatus.GetComponent<Text>().text = "I aren't master";
			}
		}

	}
}
