﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;

public class TestScript : MonoBehaviour {

	public string email;
	public string password;
	public string username;
	public string titleID = "733F";
	public string state;


	void Awake(){
		PlayFab.PlayFabSettings.TitleId = "733F";
	}


	// Use this for initialization
	void Start () {
	


	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnGUI(){
		GUILayout.BeginVertical();
		email = GUILayout.TextField(email);
		username = GUILayout.TextField(username);
		password = GUILayout.TextField(password);
		GUILayout.EndVertical();
		if(GUILayout.Button("Login")){
			Debug.Log ("Logging in");
			LoginWithEmailAddressRequest request = new LoginWithEmailAddressRequest();
			request.TitleId = titleID;
			request.Email = email;
			request.Password = password;
			PlayFabClientAPI.LoginWithEmailAddress(request,OnLoginResult,OnPlayFabError);

		}
		if(GUILayout.Button ("Register")){
			Debug.Log ("Registering");
			RegisterPlayFabUserRequest request = new RegisterPlayFabUserRequest();
			request.TitleId = titleID;
			request.Email = email;
			request.Password = password;
			request.Username = username;
			PlayFabClientAPI.RegisterPlayFabUser(request, OnResiterReseult, OnPlayFabError);
		}
	}

	void OnLoginResult (LoginResult result)
	{
		Debug.Log ("Logged In: " + result.PlayFabId);
	}

	void OnPlayFabError (PlayFabError error)
	{
		Debug.Log ("Error: " +error.ErrorMessage);
	}

	void OnResiterReseult (RegisterPlayFabUserResult result)
	{
		Debug.Log ("Registered: " + result.PlayFabId);
	}
}
