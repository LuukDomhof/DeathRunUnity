﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickupScript : MonoBehaviour {

	public bool canPickup = true;

	public string pickupName;
	public string pickupTD;

	public GameObject targetPU;
	public GameObject currentPU;

	public GameObject displayText;

	public Transform dropSpot;
	public Transform handSpot;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//Checking wether the Pickup button has been pressed
		if(Input.GetButtonDown ("Pickup")){
			//If yes, check if the player currently isn't holding any item
			if(currentPU == null){
				//If not, check if the player is allowed to pickup the item
				if(canPickup){
					//If yes, check if the player is close to a pickup
					if(targetPU != null){
						//If yes, run the Pickup function, which will handle the picking up, and run the ModRot function, which will hanndle
						//The rotation of the hand object
						Pickup();
						ModRot();
					}
				}
			//If the player is currently holding an item and is wanting to pickup another item,
			}else{
				//We create a new temporarely variable
				string senderString = "Please drop your current pickup first";
				//And use that the enable the pickuptext
				EnablePickupText(senderString);
			}
		}

		//Checking wether the Drop button has been pressed
		if(Input.GetButtonDown ("Drop")){
			//If yes, we check if the player is holding any item
			if(currentPU != null){
				//If yes, we run the Drop function
				Drop();
			}
		}

	}


	//Check wether the collider of the player has entered a trigger
	void OnTriggerEnter(Collider col){
		//If yes, check if the gameobject the trigger is on has been tagged as a pickup
		if(col.gameObject.CompareTag("Pickup")){
			//If yes, set the target pickup the the gameobject we just came close to
			targetPU = col.gameObject;
			//And create a temporarely variable, to feed into the EnablePickupText
			string senderString = "Pickup " + col.gameObject.name;
			EnablePickupText(senderString);
		}
	}

	//Check wether the collider of the player has exited a trigger
	void OnTriggerExit(Collider col){
		//If yes, check if the gameobject the trigger is on has been tagged as a pickup
		if(col.gameObject.CompareTag("Pickup")){
			//If yes, set the target pickup to nothing
			targetPU = null;
			//And run the DisablePickupText method
			DisablePickupText();
		}
	}

	//We're using this to manage the picking up of the desired item
	void Pickup ()
	{
		//We store the name of the closest pickup in a variable
		pickupName = targetPU.name;
		//We run the DisablePickupText function
		DisablePickupText();
		//Then we destroy the pickup on the ground
		Destroy(targetPU);
		//We set the targetPU to nothing
		targetPU = null;
		//Then we spawn a new version of the pickup, without any colliders or physics, into the players hand
		currentPU = Instantiate(Resources.Load(pickupName + " Hand"), handSpot.position, handSpot.rotation) as GameObject;
		//And then we make the Pickup that we just spawned in the hand position, a child of the hand object, so that it will move
		//with the player without more coding
		currentPU.transform.parent = handSpot.transform;
	}

	//We're using this to enable the displaying of the text telling you how to pickup stuff, including the name of the item
	void EnablePickupText (string toDisplay)
	{
		//Creating a variable called ptc and linking it to the Text component of out displayText gameobject chosen in the inspector
		var ptc = displayText.GetComponent<Text>();
		//Setting the text variable of our Text component to the following
		ptc.text = toDisplay;
		//And then finally we enable the component so it's visible
		ptc.enabled = true;
	}

	//We're using this to disable the displaying of the text telling you how to pickup stuff
	void DisablePickupText ()
	{
		//Creating a variable called ptc and linking it to the Text component of our displayText gameobject chosen in the inspector
		var ptc = displayText.GetComponent<Text>();
		//Then we disable to component making it invisible
		ptc.enabled = false;
	}

	//We're using this to drop the pickup that the player is currently holding
	void Drop ()
	{
		//We start by destroying the pickup in the hand, and setting the currentPU variable to nothing
		Destroy(currentPU);
		currentPU = null;
		//Then we spawn a new version of the pickup, with colliders and physics, into the dropSpot position, and set it's name to the original pickupName
		//Instead of it saying GONAME (Clone)
		GameObject droppedPU = Instantiate(Resources.Load(pickupName), dropSpot.position, dropSpot.rotation) as GameObject;
		droppedPU.name = pickupName;
	}

	//We're using this to modify the rotation of the hand object, depending on the model of the pickup
	void ModRot ()
	{
		//Using a switch state to determine what pickup has been picked up, and then setting the rotation of the hand to the desired rotation
		switch(pickupName){
		case "Shotgun":
			handSpot.transform.localEulerAngles = new Vector3(0,0,0);
			break;
		case "Assault Rifle":
			handSpot.transform.localEulerAngles = new Vector3(90f,73.38135f,0f);
			break;
		case "Rifle":
			handSpot.transform.localEulerAngles = new Vector3(90f,73.38135f,0f);
			break;
		case "Rifle With Scope":
			handSpot.transform.localEulerAngles = new Vector3(90f,73.38135f,0f);
			break;
		case "Bow":
			handSpot.transform.localEulerAngles = new Vector3(90f,258.439f,0f);
			break;
		}
	}
}
