﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class StartScreen : MonoBehaviour {

	public int newsCount = 10;

	public string[] splitNewsTitle;
	public string[] infoString;

	public string receivedUpdate;
	public string currentVersion;
	public string downloadURL;

	public GameObject newsHead;
	public GameObject newsBody;

	public GameObject updateHead;
	public GameObject updateBody;
	public GameObject playButton;

	public GameObject downloadButton;


	// Use this for initialization
	void Start () {

/*		infoString[0] = ("Client up to date");
		infoString[1] = ("Your client is up to date. Click play to start playing");
		infoString[2] = ("Client outdated");
		infoString[3] = ("Your client is outdated. Please download the patcher from http://lukewaffel.itch.io/project-deathrun");*/

		GetTitleNewsRequest request = new GetTitleNewsRequest();

		request.Count = newsCount;

		PlayFabClientAPI.GetTitleNews(request, GetTitleNewsCallBack, OnPlayFabError);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void CompareVersions ()
	{
		splitNewsTitle = receivedUpdate.Split("V"[0]);
		if(splitNewsTitle[1] == currentVersion){
			ClientUpToDate();
			Debug.Log("Client up to date");
		}else{
			ClientOutDated();
			Debug.Log ("Client out of date");
		}
	}

	void GetTitleNewsCallBack (GetTitleNewsResult result)
	{
		for(int i = 0;i < newsCount;i++){
			if(result.News[i].Title.Contains("Update:")){
				Debug.Log ("Found an update");
				newsHead.GetComponent<Text>().text = result.News[i].Title;
				newsBody.GetComponent<Text>().text = result.News[i].Body;
				receivedUpdate = result.News[i].Title;
				CompareVersions();
				break;
			}else{
				Debug.Log ("Didn't find an update");
			}
		}
	}

	void OnPlayFabError (PlayFabError error)
	{
		Debug.Log(error.ErrorMessage);
	}

	void ClientUpToDate ()
	{
		updateHead.GetComponent<Text>().text = "Client up to date";
		updateBody.GetComponent<Text>().text = "Your client is up to date. Click play to start playing";
		playButton.GetComponent<Button>().interactable = true;
		downloadButton.GetComponent<Button>().interactable = false;
	}

	void ClientOutDated ()
	{
		updateHead.GetComponent<Text>().text = "Client outdated";
		updateBody.GetComponent<Text>().text = "Your client is outdated. Please download the most recent patcher.";
		playButton.GetComponent<Button>().interactable = false;
		downloadButton.GetComponent<Button>().interactable = true;
	}

	public void OpenURL(){
		Application.OpenURL(downloadURL);
	}
}
