﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;

public class LogInScript : MonoBehaviour {

	public string titleID;
	public string username;
	public string password;
	public UserDataRecord outvalue;
	public GameObject usernameText;
	public GameObject passwordText;
	public GameObject data;

	public GameObject errorString;

	public string levelToLoad = "GameLobby";
	public List<string> keys = new List<string>();


	void Awake(){
		PlayFab.PlayFabSettings.TitleId = "733F";
	}

	// Use this for initialization
	void Start () {
	
		data = GameObject.Find("_Data");

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void LogIn(){

		username = usernameText.GetComponent<Text>().text;
		password = passwordText.GetComponent<Text>().text;

		LoginWithPlayFabRequest request = new LoginWithPlayFabRequest();
		request.TitleId = titleID;
		request.Username = username;
		request.Password = password;
		PlayFabClientAPI.LoginWithPlayFab(request,OnLoginResult,OnPlayFabError);


	}

	void OnPlayFabError (PlayFabError error)
	{
		Debug.Log ("Error: " + error.Error);
		this.gameObject.GetComponent<ErrorDisplayString>().DisplayError(errorString, error);
	}

	void OnLoginResult (LoginResult result)
	{
		Debug.Log (result.ToString());
		data.GetComponent<PlayerInfo>().playfabID = result.PlayFabId;
		data.GetComponent<PlayerInfo>().username = username;
		Application.LoadLevel(levelToLoad);
	}
}
