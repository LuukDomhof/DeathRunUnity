﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
public class RegisteringScript : MonoBehaviour {

	public string titleID;
	public string email;
	public string username;
	public string password;
	public string dispName;

	public GameObject emailText;
	public GameObject usernameText;
	public GameObject passwordText;
	public GameObject dispNameText;

	public GameObject playerInfo;

	public GameObject errorString;

	public string levelToLoad;
	void Awake(){
		PlayFab.PlayFabSettings.TitleId = "733F";
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void Register(){
		email = emailText.GetComponent<Text>().text;
		username = usernameText.GetComponent<Text>().text;
		password = passwordText.GetComponent<Text>().text;
		dispName = dispNameText.GetComponent<Text>().text;

		RegisterPlayFabUserRequest request = new RegisterPlayFabUserRequest();
		request.TitleId = titleID;
		request.Email = email;
		request.Username = username;
		request.Password = password;
		PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterResult,OnPlayFabError);
	}

	void OnRegisterResult (RegisterPlayFabUserResult result)
	{
		Debug.Log ("Registerd user: " + result.PlayFabId);
		LogIn();
	}

	void LogIn ()
	{
		LoginWithPlayFabRequest request = new LoginWithPlayFabRequest();
		request.TitleId = titleID;
		request.Username = username;
		request.Password = password;
		PlayFabClientAPI.LoginWithPlayFab(request,OnLoginResult,OnPlayFabError);
	}

	void OnLoginResult (LoginResult result)
	{
		UpdateUserDataRequest updaterequest = new UpdateUserDataRequest();
		Dictionary<string, string> datatoupdate = new Dictionary<string, string>();
		datatoupdate.Add("DispName", dispName);
		
		updaterequest.Data = datatoupdate;
		
		PlayFabClientAPI.UpdateUserData(updaterequest,UpdateUserDataCallback, OnPlayFabError);

		Debug.Log (result.ToString());
		playerInfo.GetComponent<PlayerInfo>().playfabID = result.PlayFabId;
		playerInfo.GetComponent<PlayerInfo>().username = username;
		playerInfo.GetComponent<PlayerInfo>().displayName = dispName;
		Application.LoadLevel(levelToLoad);
	}

	void OnPlayFabError (PlayFabError error)
	{
		Debug.Log ("Error: " + error.ErrorMessage);
		this.gameObject.GetComponent<ErrorDisplayString>().DisplayError(errorString, error);
	}

	void UpdateUserDataCallback (UpdateUserDataResult result)
	{
		Debug.Log("Updated to: " + result.DataVersion);
	}
}
