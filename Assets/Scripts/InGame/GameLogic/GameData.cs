﻿using UnityEngine;
using System.Collections;


public class GameData : Photon.MonoBehaviour {

	
	public enum gameStates{Waiting,Playing,Finished}
	public gameStates gameState;


	void Awake(){
		DontDestroyOnLoad(this.gameObject);
	}
	// Use this for initialization
	void Start () {
		gameState = gameStates.Waiting;
		if(PhotonNetwork.isMasterClient){
			Debug.Log ("Master");
		}else{
			Debug.Log ("Slave");
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
