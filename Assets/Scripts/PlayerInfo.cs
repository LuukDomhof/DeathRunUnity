﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;

public class PlayerInfo : MonoBehaviour {

	public string playfabID;
	public string username;
	public string displayName;
	void Awake(){

		DontDestroyOnLoad(this.gameObject);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
