﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuSwitcher : MonoBehaviour{
	
	public Transform changeDispNamePos;
	public Transform lobbyPos;
	public Slider volumeSlider;
	public Slider SFXSlider;
	public float transitionDuration = 2f;
	
	IEnumerator Transition(Transform target)
	{
		Quaternion newRot = Quaternion.Euler (target.eulerAngles);
		float t = 0f;
		Vector3 startingPos = transform.position;
		while (t < 1.0f)
		{
			t += Time.deltaTime * (Time.timeScale/transitionDuration);
			transform.position = Vector3.Lerp(startingPos, target.position, t);
			transform.rotation = Quaternion.Slerp (transform.rotation, newRot, t/10);
			yield return 0;
		}
	}
	
	void Start()
	{
		//AudioListener.volume = PlayerPrefs.GetFloat ("Volume");
		//volumeSlider.value = AudioListener.volume;
		//SFXSlider.value = PlayerPrefs.GetFloat ("SFXVolume");
		
	}
	
	public void Play()
	{
		
	}
	
	public void toDispNameChangePos()
	{
		StartCoroutine (Transition(changeDispNamePos));
	}
	
	public void Exit()
	{
		Application.Quit ();
	}
	
	public void VolumeControl(float volume)
	{
		AudioListener.volume = volume;
		PlayerPrefs.SetFloat ("Volume", volume);
	}
	
	public void SFXControl(float volume)
	{
		PlayerPrefs.SetFloat ("SFXVolume", volume);
	}
	
	public void Save()
	{
		PlayerPrefs.Save ();
	}
	
	public void toLobbyPos()
	{
		StartCoroutine (Transition(lobbyPos));
	}
}