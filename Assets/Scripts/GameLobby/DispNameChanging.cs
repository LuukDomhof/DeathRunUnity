﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using UnityEngine.UI;

public class DispNameChanging : MonoBehaviour {

	public string currentDispName;
	public string newDispName;
	public string titleID;
	public List<string> keys = new List<string>();
	public UserDataRecord dispNameOutput;

	public UserDataRecord[] values;

	public bool couldGetValues;

	public GameObject dispNameText;
	public GameObject newDispNameText;

	// Use this for initialization
	void Start () {

		keys.Add ("DispName");

	
		GetUserDataRequest request = new GetUserDataRequest();
		request.Keys = keys;

		PlayFabClientAPI.GetUserData(request, GetUserDataCallback, OnPlayFabError);

		Debug.Log ("Current keys: " +keys.Count);

	}

	void OnPlayFabError(PlayFabError error){
		Debug.Log (error.ErrorMessage);
	}

	void GetUserDataCallback(GetUserDataResult result){
		Debug.Log ("Output: " + result.Data.TryGetValue("DispName", out dispNameOutput));
		dispNameText.GetComponent<Text>().text = ("Current Display Name: " + dispNameOutput.Value);
		Debug.Log("Keys: " + result.Data.Keys.Count);
		Debug.Log(result.Data.Values.Count);
		Debug.Log ("Output vaslues: " + dispNameOutput.Value);
	}
	
	// Update is called once per frame
	void Update () {

		newDispName = newDispNameText.GetComponent<Text>().text;
	
	}

	public void ChangeDisplayName(){
		UpdateUserDataRequest updaterequest = new UpdateUserDataRequest();
		Dictionary<string, string> datatoupdate = new Dictionary<string, string>();
		datatoupdate.Add("DispName", newDispName);
		
		updaterequest.Data = datatoupdate;
		
		PlayFabClientAPI.UpdateUserData(updaterequest,UpdateUserDataCallback, OnPlayFabError);
	}

	void UpdateUserDataCallback (UpdateUserDataResult result)
	{
		Debug.Log ("Changed to version: " + result.DataVersion);
	}
}
