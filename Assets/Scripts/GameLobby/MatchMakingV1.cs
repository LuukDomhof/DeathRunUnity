﻿using UnityEngine;
using System.Collections;

public class MatchMakingV1 : MonoBehaviour {

	public GameObject playerData;
	public string roomName;
	public string levelToLoad;

	public int maxPlayers = 5;

	void Awake(){

		PhotonNetwork.automaticallySyncScene = true;
		playerData = GameObject.Find("_PlayerData");

		if(PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated){
			PhotonNetwork.ConnectUsingSettings("0.2");
		}

		if(string.IsNullOrEmpty(PhotonNetwork.playerName)){
			PhotonNetwork.playerName = playerData.GetComponent<PlayerInfo>().username;
		}
	}
	// Use this for initialization
	void Start () {
		roomName = "Room" + Random.Range(0,999);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
	}

	public void JoinRoom(){
		Debug.Log ("Starting room joining sequence.");
		PhotonNetwork.JoinRandomRoom();
	}

	public void CreateRoom(){
		Debug.Log ("Creating room");
		//PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() { maxPlayers = 2 }, null);
		PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() {maxPlayers = (byte)maxPlayers}, null);
	}
	void OnPhotonRandomJoinFailed(){
		Debug.Log ("No available rooms, creating one.");
		PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() { maxPlayers = (byte)maxPlayers }, null);
	}

	void OnCreatedRoom(){
		Debug.Log ("Created room, loading level");
		PhotonNetwork.LoadLevel("Level1");

	}

	void OnJoinedRoom(){
		Debug.Log ("Joined");
	}
}
